/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.99896914655643, "KoPercent": 0.0010308534435659283};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9837176698588762, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9989878542510121, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.9987875848690592, 500, 1500, "getDashboardData"], "isController": false}, {"data": [0.18041871921182265, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [1.0, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.9998886579572447, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [0.9994939271255061, 500, 1500, "me"], "isController": false}, {"data": [1.0, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [1.0, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [0.5314341846758349, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.999811931091552, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [1.0, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [1.0, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.05588235294117647, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [1.0, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9998886992654151, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [0.9993804213135068, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.9990272373540856, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 97007, 1, 0.0010308534435659283, 77.13673240075363, 2, 9893, 17.0, 87.0, 214.0, 1645.9700000000048, 321.13016419491527, 1527.4340128494523, 340.5012486344677], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllConfigByCategory", 988, 1, 0.10121457489878542, 7.266194331983809, 2, 167, 5.0, 11.0, 18.0, 54.66000000000008, 3.3062276210554495, 3.092173408585149, 2.964867017827862], "isController": false}, {"data": ["getDashboardData", 4124, 0, 0.0, 230.28516003879753, 96, 821, 225.0, 298.0, 325.75, 405.0, 13.784436021365208, 119.65805839249543, 36.69567636156402], "isController": false}, {"data": ["getChildCheckInCheckOutByArea", 812, 0, 0.0, 1600.213054187193, 593, 2500, 1596.0, 1956.7, 2048.7, 2257.61, 2.702297951644841, 14.576122768191423, 8.97247366757076], "isController": false}, {"data": ["getAllClassInfo", 807, 0, 0.0, 27.32342007434944, 6, 415, 15.0, 58.200000000000045, 91.79999999999973, 187.67999999999984, 2.704505162689223, 1.53713086395032, 2.643759441261633], "isController": false}, {"data": ["findAllLevels", 512, 0, 0.0, 16.037109375000007, 5, 196, 10.0, 29.0, 50.69999999999993, 92.0, 1.711189912000722, 1.0895467017817098, 1.4404743204537327], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 512, 0, 0.0, 20.455078124999993, 5, 245, 11.0, 43.69999999999999, 80.34999999999997, 176.80000000000018, 1.7115903415491898, 1.686518217405403, 1.6447313438324247], "isController": false}, {"data": ["getAllEvents", 9165, 0, 0.0, 18.878341516639356, 6, 485, 11.0, 32.0, 58.0, 146.6800000000003, 30.646639068529428, 15.173677741937912, 33.28033461348118], "isController": false}, {"data": ["addOrUpdateUserDevice", 13472, 0, 0.0, 30.960213776722124, 10, 597, 19.0, 60.0, 95.0, 208.0, 45.04028618234095, 21.728419310621508, 55.33666870570191], "isController": false}, {"data": ["getHomefeed", 181, 0, 0.0, 8006.359116022101, 5305, 9893, 7979.0, 9051.0, 9282.800000000001, 9886.44, 0.6009635337983883, 113.29395056846668, 1.8257788609831898], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 512, 0, 0.0, 26.359375000000007, 5, 353, 11.0, 66.69999999999999, 97.0, 274.48, 1.7115846198075804, 1.3672619326197275, 1.6397114375305044], "isController": false}, {"data": ["me", 988, 0, 0.0, 64.91194331983803, 24, 561, 48.0, 116.0, 149.0999999999999, 321.9900000000001, 3.3032209747845216, 5.750794774859413, 7.585528280277297], "isController": false}, {"data": ["getNotificationCount", 9340, 0, 0.0, 21.637687366166993, 7, 464, 13.0, 38.0, 65.0, 154.0, 31.232339850659926, 16.226176563038166, 33.65282528950105], "isController": false}, {"data": ["dismissChildCheckInByVPS", 807, 0, 0.0, 25.009913258983882, 7, 392, 13.0, 54.0, 88.19999999999982, 193.75999999999988, 2.7042604668619186, 1.5026603570746402, 2.0651676612168166], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 512, 0, 0.0, 31.689453125, 5, 369, 13.0, 75.09999999999997, 137.0, 278.12000000000035, 1.711498798273793, 1.398949701323403, 1.6546716897373583], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 512, 0, 0.0, 17.367187500000004, 5, 264, 10.0, 32.39999999999998, 56.69999999999993, 137.31000000000006, 1.7115846198075804, 1.6296826213988194, 1.6580976004385937], "isController": false}, {"data": ["addClassesToArea", 807, 0, 0.0, 76.41511771995046, 15, 438, 59.0, 143.0, 202.79999999999973, 332.8399999999999, 2.70430577756331, 1.4815581457158367, 1.967488090121744], "isController": false}, {"data": ["getCountCheckInOutChildren", 509, 0, 0.0, 800.6542239685659, 314, 1255, 818.0, 1003.0, 1060.0, 1148.8, 1.7086442629499456, 1.0795828497349755, 2.035689453905209], "isController": false}, {"data": ["getCentreHolidaysOfYear", 13293, 0, 0.0, 29.05927931994288, 11, 610, 18.0, 53.0, 84.0, 205.0599999999995, 44.43515903127142, 80.53883673388378, 43.984655085783295], "isController": false}, {"data": ["findAllChildrenByParent", 181, 0, 0.0, 45.033149171270686, 18, 367, 29.0, 76.00000000000006, 138.4000000000001, 331.7400000000003, 0.6122124546337042, 1.3153001955020989, 0.9380481848830877], "isController": false}, {"data": ["findAllSchoolConfig", 13475, 0, 0.0, 34.84794063079759, 8, 468, 22.0, 69.0, 102.0, 197.47999999999956, 45.03826999565493, 982.2213335380527, 26.962567813304254], "isController": false}, {"data": ["getChildCheckInCheckOut", 510, 0, 0.0, 1919.2549019607832, 879, 2864, 1944.5, 2351.9, 2433.0499999999997, 2707.8999999999996, 1.7019799700318037, 103.42520079609278, 6.924266167141107], "isController": false}, {"data": ["getClassAttendanceSummaries", 9167, 0, 0.0, 21.869859277844448, 6, 434, 12.0, 43.0, 75.0, 189.63999999999942, 30.64902238746088, 17.359797836647765, 33.612160294061106], "isController": false}, {"data": ["getLatestMobileVersion", 13477, 0, 0.0, 15.86925873710757, 8, 734, 13.0, 22.0, 29.0, 73.0, 44.93831277092364, 24.432515916763922, 26.383179158677894], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 512, 0, 0.0, 16.902343749999996, 5, 312, 10.0, 33.0, 54.349999999999966, 123.83000000000004, 1.7116075070035501, 1.1817446361831152, 1.6547767889975729], "isController": false}, {"data": ["getAllCentreClasses", 511, 0, 0.0, 32.51663405088063, 11, 327, 25.0, 53.0, 74.39999999999986, 128.76, 1.7078192051120944, 2.343248030451653, 1.9379745276760292], "isController": false}, {"data": ["getAllArea", 807, 0, 0.0, 52.610904584882284, 11, 546, 32.0, 102.40000000000009, 162.5999999999999, 334.4799999999992, 2.7043510897831156, 5.015197968259899, 2.659454636144138], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 514, 0, 0.0, 53.3443579766537, 13, 728, 27.0, 125.5, 172.75, 318.3500000000016, 1.7129793176076944, 1.1358524967340082, 1.6544302198379002], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, 100.0, 0.0010308534435659283], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 97007, 1, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["findAllConfigByCategory", 988, 1, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
